from datetime import datetime

import pytz
import sys

from proj.tasks import capture_video_and_post
from settings.base import SECTION_ID, TIME_ZONE, SERVER
from utils.helpers import get_lectures_schedule, connection

if not connection(SERVER):
    print("cannot connect to %s" % SERVER)
    sys.exit()


today = datetime.today()
lectures_schedule = get_lectures_schedule(SECTION_ID, today.weekday())

for schedule in lectures_schedule:
    today_date = today.date()
    start_time = datetime.combine(today_date, datetime.strptime(schedule['start_time'], '%H:%M:%S').time())
    end_time = datetime.combine(today_date, datetime.strptime(schedule['end_time'], '%H:%M:%S').time())
    record_duration = end_time - start_time
    record_duration = record_duration.total_seconds()

    countdown = start_time - datetime.now()
    print(countdown.total_seconds())
    capture_video_and_post.apply_async((record_duration, schedule['id']), countdown=countdown.total_seconds())
