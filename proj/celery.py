from __future__ import absolute_import, unicode_literals
from celery import Celery

from settings.base import CELERY_BROKER, CELERY_BACKEND

app = Celery('proj',
             broker=CELERY_BROKER,
             backend=CELERY_BACKEND,
             include=['proj.tasks'])

# Optional configuration, see the application user guide.
# app.conf.update(
#     result_expires=3600,
# )

if __name__ == '__main__':
    app.start()