from __future__ import absolute_import, unicode_literals

import os
import subprocess

import datetime
import requests
import uuid

from proj.celery import app
from settings.base import PRODUCTION, SERVER, VIDEO_POST_URL
from utils.helpers import connection

if PRODUCTION:
    import picamera


def record_and_post(record_duration, schedule_id):
    temp_id = uuid.uuid4()
    file_path_directory = os.path.dirname(os.path.abspath(__file__))
    lecture_post_url = "http://%s%s" % (SERVER, VIDEO_POST_URL)
    if PRODUCTION:
        raw_video_name = os.path.join(file_path_directory, 'schedule_%s_%s.h264' % (schedule_id, temp_id))
        with picamera.PiCamera() as camera:
            camera.resolution = (640, 480)
            camera.start_recording(raw_video_name)
            camera.wait_recording(record_duration)
            camera.stop_recording()
        sound_file = os.path.join(file_path_directory, 'sample.wav')
        # Convert video and add sound
        output_file = os.path.join(file_path_directory, 'schedule_%s_%s.mkv' % (schedule_id, temp_id))
        cmd = 'ffmpeg -y -i %s  -r 30 -i %s  -filter:a aresample=async=1 -c:a flac -c:v copy %s' % (
            sound_file, raw_video_name, output_file)
        subprocess.call(cmd, shell=True)  # "Muxing Done
        print('Mixing Done')
    else:
        output_file = os.path.join(file_path_directory, 'sample.mp4')

    payload = {'schedule': schedule_id}
    files = {'video': open(output_file, 'rb')}

    response = requests.post(lecture_post_url, files=files, data=payload)

    print(response.status_code)

    if PRODUCTION:
        os.remove(raw_video_name)
        os.remove(output_file)


@app.task
def capture_video_and_post(record_duration, schedule_id):
    print(record_duration, schedule_id)
    if connection(SERVER):
        record_and_post(record_duration, schedule_id)
    else:
        print("cannot connect to %s" % SERVER)
