Development Installation Instructions
=====================================

**First of all setup project ettings**
> Project setting variables are  placed inside *pieclassroom/settings/base.py*

```python
SECTION_ID = 2
SERVER = "localhost:8000"
```
> **SECTION_ID** defines the id of the section defining the classroom
> **SERVER** defines the ip and port of the server in which raspberrypi posts the video.

**Start redis server over new terminal tab by this following command**
```bash
redis-server
```

**Activate virtual environment and goto *pieclassroom* folder and then run celery worker by this command on new terminal tab**

```bash
celery -A proj purge      # optional
celery -A proj worker -l info
```