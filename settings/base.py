import os

SECTION_ID = 2
SERVER = "localhost:8000"

TIME_ZONE = 'Asia/Kathmandu'

# get url
SCHEDULE_URL = '/classroom/api/lecture-schedule/' #?section=?
VIDEO_POST_URL ='/classroom/api/lecture-create/' # video and schedule as schedule id

POST_TOKEN = 'token here'

CELERY_BACKEND = 'redis://localhost:6379/0'
CELERY_BROKER = 'redis://localhost:6379/0'

PRODUCTION = False

# VIDEO_PATH = os.path.join(BASE_DIR, 'video')