import json

import requests
import sys

import socket

from settings.base import SERVER, SCHEDULE_URL


def get_lectures_schedule(section_id, weekday):
    url = "http://%s%s" % (SERVER, SCHEDULE_URL) + '?section=%s&day=%s' % (section_id, weekday)
    response = requests.get(url)
    if response.status_code != 200:
        print("Error cannot get schedule")
        sys.exit()
    return response.json()


def connection(server):
    host, port = server.split(':')
    try:
        socket.setdefaulttimeout(10)
        socket.socket(socket.AF_INET, socket.SOCK_STREAM).connect((host, int(port)))
        return True
    except Exception as ex:
        return False
